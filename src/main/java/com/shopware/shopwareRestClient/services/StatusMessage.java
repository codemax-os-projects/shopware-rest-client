package com.shopware.shopwareRestClient.services;

public enum StatusMessage {

	TxtSuccess(200, "#c0ffaf", "statusmsg1"),
	TxtNotFound(400, "#eea782", "errmsg1"),
	ArticleNumbersWrong(1500, "#eea782", "errmsg2"), 
	ArticleNotFetched(1401, "#eea782", "errmsg3"), 
	ArticleCodeEmpty(1400, "#eea782", "errmsg4"),
	ArticleResponseNotMappable(1600, "#eea782", "errmsg5"),
	ShopNotResolvable(1700, "#eea782", "errmsg6"), 
	ArticleNotUpdated(1800, "#eea782", "errmsg7");

	private int statusCode;
	private String uiColour;
	private String statusMessage;

	StatusMessage(int statusCode, String uiColour, String statusMessage) {
		this.statusCode = statusCode;
		this.uiColour = uiColour;
		this.statusMessage = statusMessage;
	}

	public String getUiColour() {
		return uiColour;
	}
	public String getStatusMessage() {
		return statusMessage;
	}
}
