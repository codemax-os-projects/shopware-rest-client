package com.shopware.shopwareRestClient.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.shopware.shopwareRestClient.dto.article.Article;

public class QualityGate {

	Logger LOG = LoggerFactory.getLogger(QualityGate.class);

	public boolean articleQualityCheck(Article article) {
		
		if( checkArticlenumberEqualsEAN(article) && checkLengthEAN(article)) {
			return true;
		}

		return false;
	}

	/**
	 * Check if Length of EAN-Number equals 8 or 13
	 * 
	 * @param article
	 * @return
	 */
	private boolean checkLengthEAN(Article article) {
		int eanLength = article.getData().getMainDetail().getEan().length();
		if(eanLength == 8 || Integer.compare(eanLength, 13) == 0){
			return true;
		}
		LOG.error("Expected EAN with size 8 or 13. Actual size is {}", eanLength);
		return false;
	}

	/**
	 * Check if Articlenumber equal to EAN-Number
	 * 
	 * @param article
	 * @return
	 */
	private boolean checkArticlenumberEqualsEAN(Article article) {
		if (article == null) {
			LOG.error("Der Artikel ist null!");
			return false;
		}
		if (article.getData() == null) {
			LOG.error("Die Daten innerhalb des Artikels sind null: {}", article.toString());
			return false;
		}
		
		LOG.info(article.getData().getMainDetail().toString());
		
		boolean equal = article.getData().getMainDetail().getNumber()
				.equals(article.getData().getMainDetail().getEan());

		return equal;
	}

}
