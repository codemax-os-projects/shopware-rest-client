package com.shopware.shopwareRestClient.services;

import javax.mail.MessagingException;
import javax.mail.Session;


public interface MailService {

	public void send(String address, String subject, String text) throws MessagingException;

	public void postMail(Session session, String recipient, String subject, String message) throws MessagingException;
}
