package com.shopware.shopwareRestClient.services.exception;

public class UpdateShopArticleException extends Exception {

	private int statusCode;

	private static final long serialVersionUID = 1L;

	public UpdateShopArticleException(String message, int status) {
		super(message);
		setStatusCode(status);
	}

	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

}
