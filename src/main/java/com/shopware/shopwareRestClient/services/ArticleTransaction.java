package com.shopware.shopwareRestClient.services;

import com.shopware.shopwareRestClient.dto.transaction.ArticleTransactionInfoDto;

public interface ArticleTransaction {

	ArticleTransactionInfoDto articleTransaction(String codeInput, boolean verkauf, Integer amount);
	
	ArticleTransactionInfoDto searchArticle(String codeInput);
}
