package com.shopware.shopwareRestClient.services;

import javax.ws.rs.ProcessingException;
import javax.ws.rs.core.Response;

import com.shopware.shopwareRestClient.Configuration;
import com.shopware.shopwareRestClient.dto.article.Article;
import com.shopware.shopwareRestClient.dto.article.Data;
import com.shopware.shopwareRestClient.dto.article.ArticleVariants;
import com.shopware.shopwareRestClient.dto.article.Image;
import com.shopware.shopwareRestClient.dto.article.BuySell.PostArticleDto;
import com.shopware.shopwareRestClient.dto.article.BuySell.PostArticleVariantsDto;
import com.shopware.shopwareRestClient.dto.media.Media;
import com.shopware.shopwareRestClient.dto.transaction.ArticleTransactionInfoDto;
import com.shopware.shopwareRestClient.rest.ShopwareRestCallArticle;
import com.shopware.shopwareRestClient.rest.ShopwareRestCallMedia;
import com.shopware.shopwareRestClient.rest.ShopwareRestCallVariants;
import com.shopware.shopwareRestClient.services.exception.UpdateShopArticleException;
import com.shopware.shopwareRestClient.util.ArticleGetToPostMapper;

public class ArticleTransactionService {

	ShopwareRestCallArticle articleCaller;
	ShopwareRestCallVariants variantsCaller;
	ShopwareRestCallMedia mediaCaller;

	public ArticleTransactionService() {
		articleCaller = new ShopwareRestCallArticle(Configuration.getLoginData().get("user"),
				Configuration.getLoginData().get("password"));
		variantsCaller = new ShopwareRestCallVariants(Configuration.getLoginData().get("user"),
				Configuration.getLoginData().get("password"));
		mediaCaller = new ShopwareRestCallMedia(Configuration.getLoginData().get("user"),
				Configuration.getLoginData().get("password"));
	}

	/**
	 * Update stock of an Article by ID/EAN
	 * 
	 * @param codeInput
	 * @param article
	 * @param isSell
	 * @return Article
	 */
	public Article updateArticles(String codeInput, Article article, boolean isSell, Integer amount) {
		if (isSell) {
			article.getData().getMainDetail().setInStock(article.getData().getMainDetail().getInStock() - amount);
		} else {
			article.getData().getMainDetail().setInStock(article.getData().getMainDetail().getInStock() + amount);
		}
		return article;
	}

	/**
	 * Update stock of a Variant by ID/EAN
	 * 
	 * @param codeInput
	 * @param article
	 * @param isSell
	 * @return Article
	 */
	public Article updateVariants(String codeInput, Article article, boolean isSell, Integer amount) {
		ArticleVariants[] detailList = article.getData().getDetails();
		for(ArticleVariants av : detailList) {
			if (codeInput.equals(av.getEan())) {
				if (isSell) {
					av.setInStock(av.getInStock() - amount);
				} else {
					av.setInStock(av.getInStock() + amount);
				}
				break;
			}
		}
		article.getData().setDetails(detailList);
		return article;
	}

	/**
	 * 
	 * @param codeInput
	 * @param article
	 * @param isVariants
	 * @throws UpdateShopArticleException
	 */
	public ArticleTransactionInfoDto sendArticle(String codeInput, Article article, boolean isVariants)
			throws UpdateShopArticleException {
		PostArticleDto putDto = new ArticleGetToPostMapper().postArticleDtoMapper(article);
		Response response = articleCaller.put(codeInput, true, putDto);

		if(response.getStatus() >= 400) {
			throw new UpdateShopArticleException(response.getEntity().toString(), response.getStatus());
		}
		
		return fillArticleInfoDto(article.getData(), putDto.getMainDetail().getInStock());
	}

	/**
	 * 
	 * @param codeInput
	 * @param article
	 * @param variants
	 * @param isVariants
	 * @return ArticleTransactionInfoDto
	 * @throws UpdateShopArticleException
	 */
	public ArticleTransactionInfoDto sendVariants(String codeInput, ArticleVariants variants, boolean isVariants,
			Article article) throws UpdateShopArticleException {
		PostArticleVariantsDto putDto = new ArticleGetToPostMapper().postArticleVariantsDtoMapper(variants);
		Response response = variantsCaller.put(codeInput, true, putDto);

		if(response.getStatus() >= 400) {
			throw new UpdateShopArticleException(response.getEntity().toString(), response.getStatus());
		}
		
		return fillVariantsToInfoDto(variants, article);
	}

	/**
	 * Fills InfoDto with newest Article-Data for Mask-Output
	 * 
	 * @param data
	 * @param newStockAmount
	 * @return ArticleTransactionInfoDto
	 */
	protected ArticleTransactionInfoDto fillArticleInfoDto(Data data, int newStockAmount) {
		ArticleTransactionInfoDto infoDto = new ArticleTransactionInfoDto();
		// get Image Date
		Image[] imageArray = data.getImages();
		if (imageArray != null && imageArray.length > 0) {
			// get First Image of Article
			Integer mediaId = imageArray[0].getMediaId();
			if (mediaId != 0) {
				Response response = mediaCaller.getMediaOfArticle(mediaId);
				response.bufferEntity();
				String url = response.readEntity(Media.class).getData().getPath();
				infoDto.setImageUrl(url);
			}
		} else {
			infoDto.setImageUrl(null);
		}
		infoDto.setArticleName(data.getName());
		infoDto.setArticleStock(newStockAmount);
		infoDto.setEan(data.getMainDetail().getEan());
		infoDto.setLongDescription(data.getDescriptionLong());
		infoDto.setSellingPrice((double) data.getMainDetail().getPrices()[0].getPrice());

		return infoDto;
	}

	/**
	 * Fills InfoDto with newest Variants-Data for Mask-Output
	 * 
	 * @param variants
	 * @param article
	 * @return ArticleTransactionInfoDto
	 */
	ArticleTransactionInfoDto fillVariantsToInfoDto(ArticleVariants variants, Article article) {
		ArticleTransactionInfoDto infoDto = new ArticleTransactionInfoDto();
		int mediaId = 0;

		for (Image articleImage : article.getData().getImages()) {
			if (variants.getEan().equals(articleImage.getPath())) {
				mediaId = articleImage.getMediaId();
			}
		}

		if (mediaId != 0) {
			Response response = mediaCaller.getMediaOfArticle(mediaId);
			response.bufferEntity();
			String url = "";
			try {
				url = response.readEntity(Media.class).getData().getPath();
			} catch (ProcessingException e) {
				url = "AN ERROR OCCURRED. PLEASE CONTACT IT-SUPPORT.";
			}
			infoDto.setImageUrl(url);
		} else {
			infoDto.setImageUrl(null);
		}

		infoDto.setArticleName(article.getData().getName());
		infoDto.setArticleStock(variants.getInStock());
		infoDto.setEan(variants.getEan());
		infoDto.setLongDescription(variants.getDescriptionLong());
		infoDto.setSellingPrice(article.getData().getMainDetail().getPrices()[0].getPrice());

		return infoDto;
	}
}