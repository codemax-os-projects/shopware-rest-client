package com.shopware.shopwareRestClient.rest;

import javax.ws.rs.ProcessingException;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.shopware.shopwareRestClient.Configuration;
import com.shopware.shopwareRestClient.dto.article.Article;
import com.shopware.shopwareRestClient.dto.article.BuySell.PostArticleDto;
import com.shopware.shopwareRestClient.util.GenericSerializer;

public class ShopwareRestCallArticle extends ShopwareRestCall {

	public ShopwareRestCallArticle(String user, String password) {
		super(user, password);
	}

	/**
	 * GET Article by ID/ArticleNumber
	 * 
	 * @param idParameter
	 * @param isArticleNumber
	 * @return
	 * @throws JsonMappingException
	 */
	public Article getArticleById(String idParameter, boolean isArticleNumber) throws ProcessingException {
		String target = Configuration.getUriToShopware() + "api/articles/" + idParameter + "?considerTaxInput=true";
		if (isArticleNumber) { target = target + "&useNumberAsId=true"; }

		final Response response = client.target(target).request().get();
		response.bufferEntity();
		return (Article) new GenericSerializer().deserializer(Article.class, response.readEntity(String.class));
	}

	public Response put(String idParameter, boolean isArticleNumber, PostArticleDto articleDto) {
		WebTarget webTarget = null;
		if (isArticleNumber) {
			webTarget = client
					.target(Configuration.getUriToShopware() + "api/articles/" + idParameter + "?useNumberAsId=true");
		} else {
			webTarget = client.target(Configuration.getUriToShopware() + "api/articles/" + idParameter);
		}
		final Response response = webTarget.request()
				.put(Entity.entity(new GenericSerializer().serializer(articleDto), MediaType.APPLICATION_JSON));
		return response;
	}
}