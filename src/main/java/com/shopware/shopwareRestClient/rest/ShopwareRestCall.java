package com.shopware.shopwareRestClient.rest;

import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;

import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class ShopwareRestCall {

	private static final Logger LOG = LoggerFactory.getLogger(ShopwareRestCall.class);

	protected final Client client;

	public ShopwareRestCall(String user, String password) {

		TrustManager[] certs = new TrustManager[] { new X509TrustManager() {
			@Override
			public X509Certificate[] getAcceptedIssuers() {
				return null;
			}

			@Override
			public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
			}

			@Override
			public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
			}
		} };

		SSLContext ctx = null;
		try {
			ctx = SSLContext.getInstance("TLS");
			ctx.init(null, certs, new SecureRandom());
		} catch (java.security.GeneralSecurityException e) {
			LOG.error("Error while create rest-client.", e);
		}

		if (ctx != null) {
			HttpsURLConnection.setDefaultSSLSocketFactory(ctx.getSocketFactory());
		}
		
		this.client = ClientBuilder.newBuilder().sslContext(ctx).hostnameVerifier(new HostnameVerifier() {
			@Override
			public boolean verify(String hostname, SSLSession session) {
				return true;
			}
		}).register(HttpAuthenticationFeature.digest(user, password)).build();
	}
}
