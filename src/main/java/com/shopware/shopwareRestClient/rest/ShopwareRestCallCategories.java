package com.shopware.shopwareRestClient.rest;

import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.shopware.shopwareRestClient.Configuration;
import com.shopware.shopwareRestClient.dto.categories.Categories;
import com.shopware.shopwareRestClient.util.GenericSerializer;

public class ShopwareRestCallCategories extends ShopwareRestCall {

	private static final Logger LOG = LoggerFactory.getLogger(ShopwareRestCallCategories.class);
	
	public ShopwareRestCallCategories(String user, String password) {
		super(user, password);
	}

	public Categories getJsonById(String idParameter) {
		final Response response = client.target(Configuration.getUriToShopware() + "api/categories/" + idParameter).request().get();
		response.bufferEntity();
		return (Categories) new GenericSerializer().deserializer(Categories.class, response.readEntity(String.class));
	}

}