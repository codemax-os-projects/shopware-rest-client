package com.shopware.shopwareRestClient.util;

import com.shopware.shopwareRestClient.dto.article.BuySell.PostArticleDto;
import com.shopware.shopwareRestClient.dto.article.BuySell.PostArticleVariantsDto;
import com.shopware.shopwareRestClient.dto.article.BuySell.SellMaindetail;
import com.shopware.shopwareRestClient.dto.article.Article;
import com.shopware.shopwareRestClient.dto.article.ArticleVariants;
import com.shopware.shopwareRestClient.dto.article.Data;

public class ArticleGetToPostMapper {

	/**
	 * Map GET-Article-Dto auf POST-Article-Dto
	 * @param article
	 * @return
	 */
	public PostArticleDto postArticleDtoMapper(Article article){
		PostArticleDto putDto = new PostArticleDto();
		
		Data articleData = article.getData();

		SellMaindetail mpmd = new SellMaindetail();
		mpmd.setId(articleData.getMainDetail().getId());
		mpmd.setNumber(articleData.getMainDetail().getNumber());
		mpmd.setInStock(articleData.getMainDetail().getInStock());
		mpmd.setEan(articleData.getMainDetail().getEan());
		putDto.setMainDetail(mpmd);
		putDto.setName(articleData.getName());
		return putDto;
	}

	public PostArticleVariantsDto postArticleVariantsDtoMapper(ArticleVariants variants) {
		PostArticleVariantsDto variantsPostDto = new PostArticleVariantsDto();
		
		variantsPostDto.setId(variants.getId());
		variantsPostDto.setArticleId(variants.getArticleId());
		variantsPostDto.setUnitId(variants.getUnitId());
		variantsPostDto.setInStock(variants.getInStock());
		variantsPostDto.setEan(variants.getEan());
		
		return variantsPostDto;
	}
}