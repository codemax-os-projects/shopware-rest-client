package com.shopware.shopwareRestClient.util;

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.opencsv.CSVWriter;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import com.shopware.shopwareRestClient.dto.transaction.LogTransactionDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CSVParser {

	private static final Logger LOG = LoggerFactory.getLogger(CSVParser.class);

	private static final String DEFAULT_SEPARATOR = ";";
	private static Reader reader;
	private static Writer writer;
	private static CSVWriter csvWriter;

	public CSVParser(Path logfilePath) {
		try {
			writer = Files.newBufferedWriter(logfilePath);
			csvWriter = new CSVWriter(writer);
			reader = Files.newBufferedReader(logfilePath);
		} catch (IOException e) {
			LOG.error("Error while CSV-File was parsed -> " + e.getMessage());
		}
	}

	public static void writeEntryToCsv(LogTransactionDto ltDto) {
		csvWriter.writeNext(new String[] { ltDto.getLoggedDate().toString(), ltDto.getLoggedEan(),
				ltDto.getLoggedAmount().toString(), ltDto.getLoggedArticleName(), ltDto.getLoggedArticleLink() });

		try {
			csvWriter.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static List<LogTransactionDto> readCsvToEntryList(String file) {
		CsvToBean<LogTransactionDto> csvToBean = new CsvToBeanBuilder<LogTransactionDto>(reader)
				.withType(LogTransactionDto.class).withIgnoreLeadingWhiteSpace(true).build();

		Iterator<LogTransactionDto> logIterator = csvToBean.iterator();
		List<LogTransactionDto> ltdList = new ArrayList<>();
		while (logIterator.hasNext()) {
			LogTransactionDto ltd = logIterator.next();
			ltdList.add(ltd);
		}
		return ltdList;
	}

	public static void writeLine(String[] values) throws IOException {

		boolean first = true;

		StringBuilder sb = new StringBuilder();
		for (String value : values) {
			if (!first) {
				sb.append(DEFAULT_SEPARATOR);
			}
			sb.append(followCVSformat(value));
			first = false;
		}
		sb.append("\n");
		writer.append(sb.toString());
	}

	private static String followCVSformat(String value) {

		String result = value;
		if (result.contains("\"")) {
			result = result.replace("\"", "\"\"");
		}
		return result;

	}
}