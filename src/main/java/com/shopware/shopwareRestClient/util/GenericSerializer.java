package com.shopware.shopwareRestClient.util;

import java.io.IOException;

import javax.mail.MessagingException;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.shopware.shopwareRestClient.Configuration;
import com.shopware.shopwareRestClient.services.MailServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class GenericSerializer {

	private static final Logger LOG = LoggerFactory.getLogger(GenericSerializer.class);

	/**
	 * Mapping a Json-String on an object
	 * @param dtoClass
	 * @param jsonString
	 * @return
	 */
	public Object deserializer(Class<?> dtoClass, String jsonString) {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(JsonParser.Feature.ALLOW_SINGLE_QUOTES, true);
		mapper.configure(DeserializationFeature.ACCEPT_EMPTY_ARRAY_AS_NULL_OBJECT, true);
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		Object object = null;
		try {
			object = mapper.readValue(jsonString, dtoClass);
		} catch (JsonParseException e) {
			LOG.error(e.getMessage());
			try {
				new MailServiceImpl().send(Configuration.getMaintainerMail(), "Deserialization-Error", e.getMessage());
			} catch (MessagingException e1) {
				LOG.error("Wasn't able to send exception-mail because of a failure in deserializing articles!");
			}
		} catch (IOException e) {
			LOG.error(e.getMessage());
		}
		return object;
	}
	
	/**
	 * Generates a Json-String out of an Object 
	 * @param dtoObject
	 * @return
	 */
	public  String serializer(Object dtoObject){
		String jsonString = null;
		
		ObjectMapper mapper = new ObjectMapper();
		try {
			jsonString = mapper.writeValueAsString(dtoObject) ;
		} catch (JsonProcessingException e) {
			LOG.error(e.getMessage());
		}
		
		return jsonString;
	}
}
