package com.shopware.shopwareRestClient.fx;

import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.util.ResourceBundle;

import org.apache.commons.configuration.CompositeConfiguration;

import com.shopware.shopwareRestClient.dto.transaction.ArticleTransactionInfoDto;
import com.shopware.shopwareRestClient.services.ArticleTransaction;
import com.shopware.shopwareRestClient.services.ArticleTransactionImpl;
import com.shopware.shopwareRestClient.util.Properties;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

public class ViewSearchController implements Initializable {

	private static final CompositeConfiguration statusMessages = Properties.load("status/de_status_msg.properties");

	private static String articleCode_search;

	@FXML
	private Tab searchingTab;
	@FXML
	private ImageView productImageView_search;
	@FXML
	private Label statusField_search;
	@FXML
	private Label articleNameField_search;
	@FXML
	private Label articleStockField_search;
	@FXML
	private Label sellingPriceField_search;
	@FXML
	private Label eanField_search;
	@FXML
	private Label longDescriptionField_search;
	@FXML
	private Hyperlink articleShoplinkField_search;

	@FXML
	private TextField codeField_search;

	@FXML
	private VBox contentView_search;

	/**
	 * initialize DataTable
	 */
	public void initialize(URL location, ResourceBundle resources) {
		searchingTab.setGraphic(ViewController.buildImage("/control/search.png"));
	}

	@FXML
	public void handleSearchInputFocus() {
		codeField_search.requestFocus();
		Platform.runLater(() -> {
			if (!codeField_search.isFocused()) {
				codeField_search.requestFocus();
				handleSearchInputFocus();
			}
		});
	}
	
	@FXML
	public void handleSearchCodeEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			ViewController.getInstance().setProgressBar(0.1);
			String enteredCode = codeField_search.getText();
			resetSearchMaskCleaner();

			resetSearchMaskCleaner();
			ViewController.getInstance().setProgressBar(0.2);

			Thread thread = new Thread() {
				public void run() {
					ArticleTransaction transaction = new ArticleTransactionImpl();
					final ArticleTransactionInfoDto infoDto = transaction.searchArticle(enteredCode);

					Platform.runLater(new Runnable() {
						public void run() {
							contentView_search
									.setStyle("-fx-background-color:" + infoDto.getStatusMsg().getUiColour() + ";");
							statusField_search.setText(ViewSearchController.statusMessages
									.getString(infoDto.getStatusMsg().getStatusMessage()));
							statusField_search.setFont(Font.font("Verdana", FontWeight.BOLD, 14));
							switch (infoDto.getStatusMsg()) {
							case TxtNotFound:
							case ArticleCodeEmpty:
							case ArticleNotFetched:
							case ArticleNumbersWrong:
							case ArticleResponseNotMappable:
							case ShopNotResolvable:
							case ArticleNotUpdated:
								ViewController.getInstance().supportWindow_btn.setDisable(false);
								ViewController.setSupportwindow(true);
								ViewController.setErrorSubject("ScannerManager Error Alert");
								ViewController.setErrorBody(ViewSearchController.statusMessages.getString(
										infoDto.getStatusMsg().getStatusMessage()) + " --> " + infoDto.toString());
								java.awt.Toolkit.getDefaultToolkit().beep();
								break;
							case TxtSuccess:
								ViewController.setSupportwindow(false);
								articleCode_search = enteredCode;
								statusField_search.setText("Gefundener Artikel: " + enteredCode);
								articleNameField_search.setText("Artikel: " + infoDto.getArticleName());
								articleStockField_search
										.setText("Anzahl: " + Integer.toString(infoDto.getArticleStock()) + " Stück");
								sellingPriceField_search.setText("Preis: " + infoDto.getSellingPrice() + " €");
								eanField_search.setText("EAN: " + infoDto.getEan());
								longDescriptionField_search.setText("Beschreibung: \n" + infoDto.getLongDescription());
								if (infoDto.getImageUrl() != null) {
									productImageView_search.setImage(new Image(infoDto.getImageUrl()));
								} else {
									productImageView_search.setImage(
											new Image(this.getClass().getResourceAsStream("/defaultArticle.png")));
								}
								articleShoplinkField_search.setText("zum Artikel im Shop");

								break;
							}

							articleShoplinkField_search.setOnAction(ev -> {
								try {
									Desktop.getDesktop().browse(URI.create(
											"https://www.maschebeimasche.com/shopware.php?sViewport=searchFuzzy&sLanguage=1&sSearch="
													+ articleCode_search));
									codeField_search.requestFocus();
								} catch (IOException e) {
									e.printStackTrace();
								}
							});

							ViewController.getInstance().setProgressBar(1.0);
						}
					});
				}
			};
			thread.start();
		}
	}

	/**
	 * Clear Text/Image/Styling
	 */
	public void resetSearchMaskCleaner() {
		codeField_search.setText("");
		productImageView_search.setImage(null);
		statusField_search.setText("");
		articleNameField_search.setText("");
		articleStockField_search.setText("");
		sellingPriceField_search.setText("");
		eanField_search.setText("");
		longDescriptionField_search.setText("");
		articleShoplinkField_search.setText("");

		// Text-styling
		statusField_search.setTextFill(Color.web("#000"));
		articleNameField_search.setTextFill(Color.web("#000"));
		articleStockField_search.setTextFill(Color.web("#000"));
		sellingPriceField_search.setTextFill(Color.web("#000"));
		eanField_search.setTextFill(Color.web("#000"));
		longDescriptionField_search.setTextFill(Color.web("#000"));

		articleNameField_search.setFont(Font.font("Verdana", FontWeight.NORMAL, 11));
		statusField_search.setFont(Font.font("Verdana", FontWeight.NORMAL, 11));
		articleStockField_search.setFont(Font.font("Verdana", FontWeight.NORMAL, 11));
		sellingPriceField_search.setFont(Font.font("Verdana", FontWeight.NORMAL, 11));
		eanField_search.setFont(Font.font("Verdana", FontWeight.NORMAL, 11));
		longDescriptionField_search.setFont(Font.font("Verdana", FontWeight.NORMAL, 11));

		contentView_search.setStyle("");
	}
}