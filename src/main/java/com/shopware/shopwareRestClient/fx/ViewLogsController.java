package com.shopware.shopwareRestClient.fx;

import java.net.URL;
import java.util.ResourceBundle;

import com.shopware.shopwareRestClient.dto.article.BuySell.LogTransactionsDto;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;

public class ViewLogsController implements Initializable {

	private ObservableList<LogTransactionsDto> logs_data;
	private static ViewLogsController instance;

	@FXML
	private Tab loggingTab;
	@FXML
	private TableView<LogTransactionsDto> logsTable;
	@FXML
	private TableColumn<LogTransactionsDto, ImageView> logsTableImgField;
	@FXML
	private TableColumn<LogTransactionsDto, String> logsTableDateField;
	@FXML
	private TableColumn<LogTransactionsDto, String> logsTableEanField;
	@FXML
	private TableColumn<LogTransactionsDto, Integer> logsTableAmountField;
	@FXML
	private TableColumn<LogTransactionsDto, String> logsTableArticleNameField;
	@FXML
	private TableColumn<LogTransactionsDto, String> logsTableArticleLinkField;

	@FXML
	private VBox contentView_logs;

	/**
	 * initialize DataTable
	 */
	public void initialize(URL location, ResourceBundle resources) {
		logsTableImgField.setCellValueFactory(new PropertyValueFactory<LogTransactionsDto, ImageView>("logTransactionImg"));
		logsTableDateField.setCellValueFactory(new PropertyValueFactory<LogTransactionsDto, String>("logTransactionDate"));
		logsTableEanField.setCellValueFactory(new PropertyValueFactory<LogTransactionsDto, String>("logTransactionEan"));
		logsTableAmountField.setCellValueFactory(new PropertyValueFactory<LogTransactionsDto, Integer>("logTransactionAmount"));
		logsTableArticleNameField.setCellValueFactory(new PropertyValueFactory<LogTransactionsDto, String>("logTransactionArticleName"));
		logsTableArticleLinkField.setCellValueFactory(new PropertyValueFactory<LogTransactionsDto, String>("logTransactionArticleLink"));

		logs_data = FXCollections.observableArrayList();

		logsTable.setItems(logs_data);
		logsTable.setEditable(false);

		ViewLogsController.instance = this;
		loggingTab.setGraphic(ViewController.buildImage("/control/logs.png"));
	}

	public static ViewLogsController getInstance() {
		return instance;
	}

	public void addTransactionLog(Image img, String date, String codeInput, Integer amount, String name, String link) {
		this.logs_data.add(new LogTransactionsDto(img, date, codeInput, amount, name, link));
	}
}