package com.shopware.shopwareRestClient.fx;

import java.net.URL;
import java.util.ResourceBundle;

import javax.mail.MessagingException;

import com.shopware.shopwareRestClient.Configuration;
import com.shopware.shopwareRestClient.services.MailService;
import com.shopware.shopwareRestClient.services.MailServiceImpl;

import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ToolBar;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;

public class ViewController implements Initializable {

	private static ViewController instance;

	private static boolean supportwindow;

	private static String errorSubject;

	private static String errorBody;

	private MailService mailService;

	@FXML
	public ImageView closeBtn;

	@FXML
	public ToolBar toolbarMenu;

	@FXML
	public Button supportWindow_btn;

	@FXML
	public ProgressBar progressBar;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		ViewController.instance = this;
		this.supportWindow_btn.setVisible(false);
		this.mailService = new MailServiceImpl();
		this.setProgressBar(0.0);

		toolbarMenu.setOnMousePressed(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				View.getInstance().setxOffset(View.getInstance().getPrimaryStage().getX() - event.getScreenX());
				View.getInstance().setyOffset(View.getInstance().getPrimaryStage().getY() - event.getScreenY());
			}
		});

		toolbarMenu.setOnMouseDragged(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				View.getInstance().getPrimaryStage().setX(event.getScreenX() + View.getInstance().getxOffset());
				View.getInstance().getPrimaryStage().setY(event.getScreenY() + View.getInstance().getyOffset());
			}
		});

		closeBtn.setImage(new Image(this.getClass().getResourceAsStream("/control/cross.png")));

		closeBtn.setOnMouseMoved(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent mouseEvent) {
				closeBtn.setImage(new Image(this.getClass().getResourceAsStream("/control/cross_pressed.png")));
			}
		});

		closeBtn.setOnMouseExited(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent mouseEvent) {
				closeBtn.setImage(new Image(this.getClass().getResourceAsStream("/control/cross.png")));
			}
		});

		closeBtn.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent mouseEvent) {
				Platform.exit();
			}
		});

	}

	public void updateProgressStatus(double progress) {
		Platform.runLater(() -> ViewController.instance.setProgressBar(progress));
	}

	public static ImageView buildImage(String imgPatch) {
		Image i = new Image(imgPatch);
		ImageView imageView = new ImageView();
		imageView.setFitHeight(26);
		imageView.setFitWidth(26);
		imageView.setImage(i);
		return imageView;
	}

	@FXML
	public void handleErrorSubmissionAction() throws MessagingException {
		supportWindow_btn.setDisable(true);
		mailService.send(Configuration.getMaintainerMail(), errorSubject, errorBody);
	}

	public void setProgressBar(double steps) {
		this.progressBar.setProgress(steps);
	}

	public static void setErrorSubject(String errorSubject) {
		ViewController.errorSubject = errorSubject;
	}

	public static synchronized void setErrorBody(String errorBody) {
		ViewController.errorBody = errorBody;
	}

	public static void setSupportwindow(boolean supportwindow) {
		ViewController.supportwindow = supportwindow;
		ViewController.instance.supportWindow_btn.setVisible(supportwindow);
	}

	public static ViewController getInstance() {
		return instance;
	}

}