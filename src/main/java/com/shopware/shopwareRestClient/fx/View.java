package com.shopware.shopwareRestClient.fx;

import com.shopware.shopwareRestClient.Configuration;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;

/**
 * ScannerManager
 * 
 * Update 7.0: - create more parameterized env by using properties and providing
 * a configurationpage. - provide advanced statistics - integrate persistence
 * by.. local DB? Filebased? - update to java 10 - documentation
 * 
 */
public class View extends Application {

	private static View instance;

	private Stage primaryStage;

	private double xOffset;

	private double yOffset;

	@Override
	public void start(Stage primaryStage) throws IOException {
		Parent root = FXMLLoader.load(this.getClass().getResource("/View.fxml"));
		Scene scene = new Scene(root);
		scene.getStylesheets().addAll(this.getClass().getResource("/style.css").toExternalForm());
		primaryStage.setScene(scene);
		primaryStage.initStyle(StageStyle.UNDECORATED);

		primaryStage.alwaysOnTopProperty();
		primaryStage.centerOnScreen();
		primaryStage.setTitle("Shopware5 Client");
		primaryStage.getIcons().add(new Image(this.getClass().getResourceAsStream("/icon.png")));
		primaryStage.minHeightProperty().setValue(730);
		primaryStage.minWidthProperty().setValue(960);
		primaryStage.maxHeightProperty().setValue(730);
		primaryStage.maxWidthProperty().setValue(960);
		primaryStage.setResizable(false);
		primaryStage.show();
		primaryStage.toFront();

		this.primaryStage = primaryStage;
		this.xOffset = primaryStage.getX();
		this.yOffset = primaryStage.getY();

		View.instance = this;
	}

	public static void main(String[] args) {
		new Configuration();
		launch(args);
	}

	public Stage getPrimaryStage() {
		return primaryStage;
	}

	public double getxOffset() {
		return xOffset;
	}

	public void setxOffset(double xOffset) {
		this.xOffset = xOffset;
	}

	public double getyOffset() {
		return yOffset;
	}

	public void setyOffset(double yOffset) {
		this.yOffset = yOffset;
	}

	public static View getInstance() {
		return instance;
	}

}
