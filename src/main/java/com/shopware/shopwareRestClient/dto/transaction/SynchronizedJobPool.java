package com.shopware.shopwareRestClient.dto.transaction;

import com.shopware.shopwareRestClient.dto.transaction.model.Job;

import java.util.List;

public class SynchronizedJobPool {

    private List<Job> jobPool;

    public boolean isArticleInQueue(String code) {
        return jobPool.stream().anyMatch(job -> job.getEnteredCode().contains(code));
    }

    public synchronized void addJob(Job job) {
        jobPool.add(job);
    }
}
