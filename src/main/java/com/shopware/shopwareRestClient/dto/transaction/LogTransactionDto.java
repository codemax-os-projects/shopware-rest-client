package com.shopware.shopwareRestClient.dto.transaction;

import java.time.LocalDateTime;

public class LogTransactionDto {

	private LocalDateTime loggedDate;

	private String loggedEan;

	private Integer loggedAmount;

	private String loggedArticleName;

	private String loggedArticleLink;

	public String[] getDtoAsStringArray() {
		return new String[] {loggedDate.toString(), loggedEan.toString(), loggedAmount.toString(),
				loggedArticleName, loggedArticleLink};
	}
	
	public LogTransactionDto(LocalDateTime loggedDate, String loggedEan, Integer loggedAmount, String loggedArticleName,
			String loggedArticleLink) {
		super();
		this.loggedDate = loggedDate;
		this.loggedEan = loggedEan;
		this.loggedAmount = loggedAmount;
		this.loggedArticleName = loggedArticleName;
		this.loggedArticleLink = loggedArticleLink;
	}

	public LogTransactionDto() {

	}

	public LocalDateTime getLoggedDate() {
		return loggedDate;
	}

	public void setLoggedDate(LocalDateTime loggedDate) {
		this.loggedDate = loggedDate;
	}

	public String getLoggedEan() {
		return loggedEan;
	}

	public void setLoggedEan(String loggedEan) {
		this.loggedEan = loggedEan;
	}

	public Integer getLoggedAmount() {
		return loggedAmount;
	}

	public void setLoggedAmount(Integer loggedAmount) {
		this.loggedAmount = loggedAmount;
	}

	public String getLoggedArticleName() {
		return loggedArticleName;
	}

	public void setLoggedArticleName(String loggedArticleName) {
		this.loggedArticleName = loggedArticleName;
	}

	public String getLoggedArticleLink() {
		return loggedArticleLink;
	}

	public void setLoggedArticleLink(String loggedArticleLink) {
		this.loggedArticleLink = loggedArticleLink;
	}

	@Override
	public String toString() {
		return "LogTransactionDto [loggedDate=" + loggedDate + ", loggedEan=" + loggedEan + ", loggedAmount="
				+ loggedAmount + ", loggedArticleName=" + loggedArticleName + ", loggedArticleLink=" + loggedArticleLink
				+ "]";
	}

}
