package com.shopware.shopwareRestClient.dto.transaction.model;

public class Job {

    private String enteredCode;
    private int amount;
    private boolean isSell;

    public Job(String enteredCode, int amount, boolean isSell) {
        this.enteredCode = enteredCode;
        this.amount = amount;
        this.isSell = isSell;
    }

    public String getEnteredCode() {
        return enteredCode;
    }

    public void setEnteredCode(String enteredCode) {
        this.enteredCode = enteredCode;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public boolean isSell() {
        return isSell;
    }

    public void setSell(boolean sell) {
        isSell = sell;
    }
}
