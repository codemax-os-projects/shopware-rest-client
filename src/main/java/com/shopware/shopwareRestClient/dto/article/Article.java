package com.shopware.shopwareRestClient.dto.article;

public class Article {

    private Data data;
    private Boolean success;
    private String message;
    
	public Data getData() {
		return data;
	}
	public void setData(Data data) {
		this.data = data;
	}
	public Boolean getSuccess() {
		return success;
	}

	@Override
	public String toString() {
		return "Article [data=" + data + ", success=" + success + ", message=" + message + "]";
	}

}