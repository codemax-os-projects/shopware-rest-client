package com.shopware.shopwareRestClient.dto.article;

import java.util.Arrays;

public class ArticleList {

	private Data data[];
	private Boolean success;
	private int total;
	private String message;
    
	public Data[] getData() {
		return data;
	}
	public void setData(Data[] data) {
		this.data = data;
	}
	public Boolean getSuccess() {
		return success;
	}
	public int getTotal() {
		return total;
	}

	@Override
	public String toString() {
		return "ArticleList [data=" + Arrays.toString(data) + ", success=" + success + ", total=" + total + ", message="
				+ message + "]";
	}
}