package com.shopware.shopwareRestClient.dto.article.BuySell;

public class PostArticleDto {

	/**
	 * Article name - Required
	 */
	private String name;

	private SellMaindetail mainDetail;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public SellMaindetail getMainDetail() {
		return mainDetail;
	}

	public void setMainDetail(SellMaindetail mainDetail) {
		this.mainDetail = mainDetail;
	}

	@Override
	public String toString() {
		return "SellArticleDto [name=" + name + ", mainDetail=" + mainDetail + "]";
	}
}