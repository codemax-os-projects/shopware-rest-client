package com.shopware.shopwareRestClient.dto.article.BuySell;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class LogTransactionsDto {
	public ImageView logTransactionImg;
	public SimpleStringProperty logTransactionDate;
	public SimpleStringProperty logTransactionEan;
	public SimpleIntegerProperty logTransactionAmount;
	public SimpleStringProperty logTransactionArticleName;
	public SimpleStringProperty logTransactionArticleLink;

	public LogTransactionsDto(Image logTransactionImg, String logTransactionDate, String logTransactionEan, Integer logTransactionAmount, String logTransactionArticleName, String logTransactionArticleLink) {
		this.logTransactionImg = new ImageView(logTransactionImg);
		//logTransactionImg.setImage(logTransactionImg);
		this.logTransactionDate = new SimpleStringProperty(logTransactionDate);
		this.logTransactionEan = new SimpleStringProperty(logTransactionEan);
		this.logTransactionAmount = new SimpleIntegerProperty(logTransactionAmount);
		this.logTransactionArticleName = new SimpleStringProperty(logTransactionArticleName);
		this.logTransactionArticleLink = new SimpleStringProperty(logTransactionArticleLink);
	}

	public ImageView getLogTransactionImg() {
		return logTransactionImg;
	}

	public void setLogTransactionImg(ImageView logTransactionImg) {
		this.logTransactionImg = logTransactionImg;
	}

	public String getLogTransactionDate() {
		return logTransactionDate.get();
	}

	public SimpleStringProperty logTransactionDateProperty() {
		return logTransactionDate;
	}

	public String getLogTransactionEan() {
		return logTransactionEan.get();
	}

	public SimpleStringProperty logTransactionEanProperty() {
		return logTransactionEan;
	}

	public int getLogTransactionAmount() {
		return logTransactionAmount.get();
	}

	public SimpleIntegerProperty logTransactionAmountProperty() {
		return logTransactionAmount;
	}

	public String getLogTransactionArticleName() {
		return logTransactionArticleName.get();
	}

	public SimpleStringProperty logTransactionArticleNameProperty() {
		return logTransactionArticleName;
	}

	public String getLogTransactionArticleLink() {
		return logTransactionArticleLink.get();
	}

	public SimpleStringProperty logTransactionArticleLinkProperty() {
		return logTransactionArticleLink;
	}
}