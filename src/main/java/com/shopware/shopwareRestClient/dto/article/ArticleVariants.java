package com.shopware.shopwareRestClient.dto.article;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ArticleVariants {

	private String number;
	private String supplierNumber;
	private String ean;
	private String purchaseUnit;
	private String descriptionLong;
	private String referenceUnit;
	private String packUnit;
	private String shippingTime;
	private Price[] prices;
	private ConfiguratorOption[] configuratorOptions;
	private Attribute attribute;
	private boolean lastStock;

	/**
	 * keys
	 */
	private Integer id;
	private Integer articleId;
	private Integer unitId;

	private Integer kind;
	private Integer inStock;
	private Integer position;
	private Integer minPurchase;
	private Integer purchaseSteps;
	private Integer maxPurchase;
	private String releaseDate;
	private boolean active;
	private boolean shippingFree;

	private Integer stockMin;
	private Image[] images;

	public String getNumber() {
		return number;
	}

	public String getSupplierNumber() {
		return supplierNumber;
	}

	public String getEan() {
		return ean;
	}

	public void setEan(String ean) {
		this.ean = ean;
	}

	public String getPurchaseUnit() {
		return purchaseUnit;
	}

	public String getDescriptionLong() {
		return descriptionLong;
	}

	public String getReferenceUnit() {
		return referenceUnit;
	}

	public String getPackUnit() {
		return packUnit;
	}

	public String getShippingTime() {
		return shippingTime;
	}

	public Price[] getPrices() {
		return prices;
	}

	public ConfiguratorOption[] getConfiguratorOptions() {
		return configuratorOptions;
	}

	public Attribute getAttribute() {
		return attribute;
	}

	public void setAttribute(Attribute attribute) {
		this.attribute = attribute;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getArticleId() {
		return articleId;
	}

	public void setArticleId(Integer articleId) {
		this.articleId = articleId;
	}

	public Integer getUnitId() {
		return unitId;
	}

	public void setUnitId(Integer unitId) {
		this.unitId = unitId;
	}

	public Integer getKind() {
		return kind;
	}

	public Integer getInStock() {
		return inStock;
	}

	public void setInStock(Integer inStock) {
		this.inStock = inStock;
	}

	public Integer getPosition() {
		return position;
	}

	public Integer getMinPurchase() {
		return minPurchase;
	}

	public Integer getPurchaseSteps() {
		return purchaseSteps;
	}

	public Integer getMaxPurchase() {
		return maxPurchase;
	}

	public String getReleaseDate() {
		return releaseDate;
	}

	public boolean isActive() {
		return active;
	}

	public boolean isShippingFree() {
		return shippingFree;
	}

	public Integer getStockMin() {
		return stockMin;
	}

	public Image[] getImages() {
		return images;
	}

	public boolean isLastStock() {
		return lastStock;
	}

	@Override
	public String toString() {
		return "Detail [number=" + number + ", supplierNumber=" + supplierNumber + ", ean=" + ean
				+ ", purchaseUnit=" + purchaseUnit + ", descriptionLong=" + descriptionLong + ", referenceUnit="
				+ referenceUnit + ", packUnit=" + packUnit + ", shippingTime=" + shippingTime + ", prices="
				+ Arrays.toString(prices) + ", configuratorOptions=" + Arrays.toString(configuratorOptions)
				+ ", attribute=" + attribute + ", id=" + id + ", articleId=" + articleId + ", unitId=" + unitId
				+ ", kind=" + kind + ", inStock=" + inStock + ", position=" + position + ", minPurchase=" + minPurchase
				+ ", purchaseSteps=" + purchaseSteps + ", maxPurchase=" + maxPurchase + ", releaseDate=" + releaseDate
				+ ", active=" + active + ", shippingFree=" + shippingFree + ", stockMin=" + stockMin + "]";
	}
}