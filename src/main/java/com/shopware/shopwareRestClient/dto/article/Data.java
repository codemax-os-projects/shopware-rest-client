package com.shopware.shopwareRestClient.dto.article;

import java.util.Arrays;
import java.util.Date;

public class Data {

	private int id;
	private Integer mainDetailId;
	private int supplierId;
	private int taxId;
	private int priceGroupId;
	private int filterGroupId;
	private int configuratorSetId;

	private String name;
	private String description;
	private String descriptionLong;
	private Date added;
	private Boolean active;
	private int pseudoSales;
	private Boolean highlight;
	private String keywords;
	private String metaTitle;
	private Boolean lastStock;
	private Boolean notification;
	private String template;
	private int mode;
	private Date availableFrom;
	private Date availableTo;
	private Maindetail mainDetail;
	private PropertyValues[] propertyValues;
	private Supplier supplier;
	private Image[] images;
	private Link[] links;
	private Category[] categories;
	private Similar[] similar;
	private Related related;
	private ArticleVariants[] details;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Integer getMainDetailId() {
		return mainDetailId;
	}

	public void setMainDetailId(Integer mainDetailId) {
		this.mainDetailId = mainDetailId;
	}

	public int getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(int supplierId) {
		this.supplierId = supplierId;
	}

	public int getTaxId() {
		return taxId;
	}

	public void setTaxId(int taxId) {
		this.taxId = taxId;
	}

	public int getPriceGroupId() {
		return priceGroupId;
	}

	public void setPriceGroupId(int priceGroupId) {
		this.priceGroupId = priceGroupId;
	}

	public int getFilterGroupId() {
		return filterGroupId;
	}

	public void setFilterGroupId(int filterGroupId) {
		this.filterGroupId = filterGroupId;
	}

	public int getConfiguratorSetId() {
		return configuratorSetId;
	}

	public void setConfiguratorSetId(int configuratorSetId) {
		this.configuratorSetId = configuratorSetId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDescriptionLong() {
		return descriptionLong;
	}

	public void setDescriptionLong(String descriptionLong) {
		this.descriptionLong = descriptionLong;
	}

	public Date getAdded() {
		return added;
	}

	public void setAdded(Date added) {
		this.added = added;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public int getPseudoSales() {
		return pseudoSales;
	}

	public void setPseudoSales(int pseudoSales) {
		this.pseudoSales = pseudoSales;
	}

	public Boolean getHighlight() {
		return highlight;
	}

	public void setHighlight(Boolean highlight) {
		this.highlight = highlight;
	}

	public String getKeywords() {
		return keywords;
	}

	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}

	public String getMetaTitle() {
		return metaTitle;
	}

	public void setMetaTitle(String metaTitle) {
		this.metaTitle = metaTitle;
	}

	public Boolean getLastStock() {
		return lastStock;
	}

	public void setLastStock(Boolean lastStock) {
		this.lastStock = lastStock;
	}

	public Boolean getNotification() {
		return notification;
	}

	public void setNotification(Boolean notification) {
		this.notification = notification;
	}

	public String getTemplate() {
		return template;
	}

	public void setTemplate(String template) {
		this.template = template;
	}

	public int getMode() {
		return mode;
	}

	public void setMode(int mode) {
		this.mode = mode;
	}

	public Date getAvailableFrom() {
		return availableFrom;
	}

	public void setAvailableFrom(Date availableFrom) {
		this.availableFrom = availableFrom;
	}

	public Date getAvailableTo() {
		return availableTo;
	}

	public void setAvailableTo(Date availableTo) {
		this.availableTo = availableTo;
	}

	public Maindetail getMainDetail() {
		return mainDetail;
	}

	public void setMainDetail(Maindetail mainDetail) {
		this.mainDetail = mainDetail;
	}

	public PropertyValues[] getPropertyValues() {
		return propertyValues;
	}

	public void setPropertyValues(PropertyValues[] propertyValues) {
		this.propertyValues = propertyValues;
	}

	public Supplier getSupplier() {
		return supplier;
	}

	public void setSupplier(Supplier supplier) {
		this.supplier = supplier;
	}

	public Image[] getImages() {
		return images;
	}

	public void setImages(Image[] images) {
		this.images = images;
	}

	public Link[] getLinks() {
		return links;
	}

	public void setLinks(Link[] links) {
		this.links = links;
	}

	public Category[] getCategories() {
		return categories;
	}

	public void setCategories(Category[] categories) {
		this.categories = categories;
	}

	public Similar[] getSimilar() {
		return similar;
	}

	public void setSimilar(Similar[] similar) {
		this.similar = similar;
	}

	public Related getRelated() {
		return related;
	}

	public void setRelated(Related related) {
		this.related = related;
	}

	public ArticleVariants[] getDetails() {
		return details;
	}

	public void setDetails(ArticleVariants[] details) {
		this.details = details;
	}
}