package com.shopware.shopwareRestClient.dto.article.BuySell;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.shopware.shopwareRestClient.dto.article.Image;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class PostArticleVariantsDto {
	
	/**
	 * 	primary key
	 */
	private int id;	
	
	/**
	 * foreign keys
	 */
	private int articleId;
	private int unitId;
	private String ean;	
	private int inStock;
	private int stockMin;
	private Image[] images;

	public String getEan() {
		return ean;
	}
	public void setEan(String ean) {
		this.ean = ean;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public void setArticleId(int articleId) {
		this.articleId = articleId;
	}
	public void setUnitId(int unitId) {
		this.unitId = unitId;
	}
	public void setInStock(int inStock) {
		this.inStock = inStock;
	}
	public int getArticleId() { return articleId; }
	public int getUnitId() { return unitId; }
	public int getInStock() { return inStock; }
	public int getStockMin() { return stockMin; }
	public Image[] getImages() { return images; }
	public void setStockMin(int stockMin) { this.stockMin = stockMin; }
	public void setImages(Image[] images) { this.images = images; }

	@Override
	public String toString() {
		return "PostArticleVariantsDto [id=" + id + ", articleId=" + articleId + ", unitId=" + unitId + ", ean=" + ean + ", inStock=" + inStock + ", stockMin=" + stockMin + ", images="
				+ Arrays.toString(images) + "]";
	}
}